import { registerEvent, registerQuery } from "../reframe";
import { assocAll, assocIn, assoc, get, getIn } from "../utils";

registerEvent("profile/load", function (db, params) {
  const username = params.profile;
  const currentProfile = getIn(db, ["profile", "data", "username"], null);
  const currentLoggedUser = getIn(db, ["user", "data"], null);
  let newDb = db;
  if (currentProfile !== username) {
    newDb = assocIn(db, ["profile", "loading"], true);
  }
  return {
    db: newDb,
    http: {
      method: "GET",
      url: "/api/profiles/" + username,
      success: ["profile/load-success"],
      fail: ["profile/load-fail"],
    },
  };
});

registerEvent("profile/load-success", function (db, result) {
  return {
    db: assocAll(db, {
      "profile/loading": false,
      "profile/data": result.profile,
    }),
  };
});

registerQuery("profile/loading?", function (db) {
  return getIn(db, ["profile", "loading"], false);
});

registerQuery("profile/data", function (db) {
    return getIn(db, ["profile", "data"], {});
});
