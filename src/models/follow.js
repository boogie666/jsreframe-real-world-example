import { registerEvent, registerQuery } from "../reframe";
import { assocAll, assocIn, assoc, get, getIn } from "../utils";

registerEvent("follow/toggle", function (db, username) {
  const following = getIn(db, ["profile", "data", "following"], false);
  const user = getIn(db, ["user", "data"], false);
  return {
    db: assocIn(db, ["follow", "loading"], false),
    http: {
      url: "/api/profiles/" + username + "/follow",
      method: following ? "DELETE" : "POST",
      body: { user: user },
      success: ["follow/toggle-success"],
      fail: ["follow/toggle-fail"],
    },
  };
});

registerEvent("follow/toggle-success", function (db, { profile }) {
  return {
    db: assocAll(db, {
      "follow/loading": false,
      "profile/data/following": profile.following,
    }),
  };
});

registerQuery("follow/loading?", function (db) {
  return getIn(db, ["follow", "loading"], false);
});
