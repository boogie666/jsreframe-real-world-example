import { useEffect } from "react";
import { dispatch } from "./reframe";

function router(e) {
  let url = window.location.hash.slice(1) || "/";
  dispatch("app/navigate", url);
}

export function useNav() {
  useEffect(function () {
    window.addEventListener("load", router);
    window.addEventListener("hashchange", router);
    return function () {
      window.removeEventListener("load", router);
      window.removeEventListener("hashchange", router);
    };
  }, []);
}
