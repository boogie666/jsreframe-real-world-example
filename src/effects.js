import { registerEffect, setState, dispatch } from "./reframe.js";
import { getIn } from "./utils";

registerEffect("db", function (context, newState) {
  setState(newState);
});

registerEffect("dispatch", function (_, payload) {
  setTimeout((_) => dispatch(payload.event, ...payload.params), 0);
});

registerEffect("dispatchN", function (_, payloads) {
  setTimeout((_) => {
    for (const payload of payloads) {
      const params = payload.params || [];
      dispatch(payload.event, ...params);
    }
  }, 0);
});

registerEffect("localStorage", function (_, payload) {
  const { method, success } = payload;
  switch (method) {
    case "delete":
      setTimeout(function () {
        localStorage.removeItem(payload.key);
        if (success) {
          const [event, ...params] = success;
          dispatch(event, ...params, payload.value);
        }
      }, 0);
      break;
    case "write":
      setTimeout(function () {
        localStorage.setItem(payload.key, payload.value);
        if (success) {
          const [event, ...params] = success;
          dispatch(event, ...params, payload.value);
        }
      }, 0);
      break;
    case "read":
      setTimeout(function () {
        if (success) {
          const [event, ...params] = success;
          dispatch(event, ...params, localStorage.getItem(payload.key));
        }
      }, 0);
      break;
    default:
      break;
  }
});

registerEffect("http", function (ctx, payload) {
  const { success, fail } = payload;
  const token = getIn(ctx, ["db", "user", "token"], null);
  fetch(payload.url, {
    ...payload,
    body: JSON.stringify(payload.body),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    },
  })
    .then(async function (response) {
      if (response.status < 200 || response.status > 299) {
        throw await response.json();
      }
      return response.json();
    })
    .then(function (result) {
      if (success) {
        const [successEvent, ...successParams] = success;
        dispatch(successEvent, ...successParams, result);
      }
    })
    .catch((e) => {
      if (fail) {
        const [failEvent, ...failParams] = fail;
        dispatch(failEvent, ...failParams, e);
      }
    });
});

registerEffect("nav", function (ctx, url) {
  setTimeout((_) => (window.location.hash = url), 0);
});
