import { useSubscription, dispatch } from "../reframe";
import { FollowButton } from "../components/FollowButton.js";
import { FavButton } from "../components/FavButton.js";
import { TagList } from "../components/TagList.js";
import { ErrorList } from "../components/ErrorList.js";
import { Comments } from "../components/Comments.js";

function ArticleMeta({ article }) {
  const { author, createdAt, slug } = article;
  const username = useSubscription("user/username");
  const loggedIn = useSubscription("user/logged-in?");
  const profile = useSubscription("profile/data");

  return (
    <div className="article-meta">
      <a href={"/#/profile/" + author.username}>
        <img src={author.image} alt="user image" />
      </a>
      <div className="info">
        <a className="author" href={"/#/profile/" + author.username}>
          {author.username}
        </a>
        <span className="date">{createdAt}</span>
        {username !== author.username ? null : (
          <span>
            <a
              className="btn btn-sm btn-outline-secondary"
              href={"/#/editor/" + slug}
            >
              <i className="ion-edit" />
              <span>Edit Article</span>
            </a>{" "}
            <a
              className="btn btn-sm btn-outline-secondary"
              href="/#/"
              onClick={() => dispatch("articles/delete", slug)}
            >
              <i className="ion-trash-a" />
              <span>Delete Article</span>
            </a>
          </span>
        )}
        {loggedIn && username !== author.username && (
          <span>
            <FollowButton username={author.username} />
            <FavButton article={article} />
          </span>
        )}
      </div>
    </div>
  );
}

export function Article() {
  const article = useSubscription("articles/active");
  if (!article.slug) {
    return null;
  }
  return (
    <div className="article-page">
      <div className="banner">
        <div className="container">
          <h1>{article.title}</h1>
          <ArticleMeta article={article} />
        </div>
      </div>
      <div className="container page">
        <div className="row article-content">
          <div className="col-md-12">
            <p>{article.body}</p>
          </div>
        </div>
        <TagList tags={article.tagList} />
        <hr />
        <div className="article-actions">
          <ArticleMeta article={article} />
        </div>
        <Comments article={article} />
      </div>
    </div>
  );
}
