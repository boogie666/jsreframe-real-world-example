import { registerEvent, registerQuery } from "../reframe";
import { getIn, assocIn } from "../utils";
import "./user";
import "./tag";
import "./article";

registerEvent("app/init", function (db) {
  return {
    db: assocIn(db, ["app", "loading"], true),
    localStorage: {
      method: "read",
      key: "user/data",
      success: ["user/init-userdata"],
    },
  };
});

registerQuery("app/loading?", function (db) {
  return getIn(db, ["app", "loading"], false);
});

registerQuery("app/active-page", function (db) {
  return getIn(db, ["app", "activePage", "page"], "");
});

function activePageFromUrl(url) {
  let [base, ...path] = url.split("/").filter((x) => !!x);
  return { page: base || "home", params: path };
}

registerEvent("app/navigate", function (db, url) {
  const { page, params } = activePageFromUrl(url);
  const loggedIn = getIn(db, ["user", "token"], null);
  const newDb = assocIn(db, ["app", "activePage"], { page, params });
  switch (page) {
    case "home":
      return {
        db: newDb,
        dispatchN: [
          { event: "tags/load" },
          loggedIn
            ? { event: "articles/load-feed", params: [{ limit: 10 }] }
            : { event: "articles/load", params: [{ limit: 10 }] },
        ],
      };
    case "profile":
      const profile = (params && params[0]) || db.user.username;
      const favorited = (params && params[1]) || null;
      return {
        db: newDb,
        dispatchN: [
          { event: "profile/load", params: [{ profile: profile }] },
          favorited
            ? { event: "articles/load", params: [{ favorited: profile }] }
            : { event: "articles/load", params: [{ author: profile }] },
        ],
      };
    case "article": {
      const slug = params && params[0];
      const author = getIn(db, ["articles", "data", slug, "author"]);
      return {
        db: assocIn(newDb, ["activeArticle", "slug"], slug),
        dispatchN: [
          { event: "profile/load", params: [{ profile: author.username }] },
          { event: "articles/load", params: [{ limit: 10 }] },
          { event: "comments/load", params: [{ slug: slug }] },
        ],
      };
    }
    case "editor": {
      const slug = params && params[0];
      if (slug === "new") {
        return {
          db: assocIn(newDb, ["activeArticle", "slug"], null),
        };
      } else {
        const { author } = getIn(db, ["articles", "data", slug], {});
        return {
          db: assocIn(newDb, ["articles", "active"], slug),
          dispatchN: [
            { event: "profile/load", params: [{ profile: author.username }] },
            { event: "comments/load", params: [{ slug }] },
          ],
        };
      }
    }
    default:
      return { db: newDb };
  }
});
