import { registerEvent, registerQuery } from "../reframe";
import { assocAll, assocIn, assoc, get, getIn, updateIn } from "../utils";

registerQuery("comments/loading?", function (db) {
  return getIn(db, ["comments", "loading"], false);
});

registerQuery("comments/post-loading?", function (db) {
  return getIn(db, ["comments", "postLoading"], false);
});

registerEvent("comments/load", function (db, { slug }) {
  return {
    db: assocIn(db, ["comments", "loading"], true),
    http: {
      method: "GET",
      url: "/api/articles/" + slug + "/comments",
      success: ["comments/load-success", slug],
      fail: ["comments/load-fail", slug],
    },
  };
});

registerEvent("comments/post", function (db, { slug, body }) {
  return {
    db: assocAll(db, {
      "comments/postLoading": true,
      "comments/new": "",
    }),
    http: {
      method: "POST",
      url: "/api/articles/" + slug + "/comments",
      body: { comment: { body } },
      success: ["comments/post-success", slug],
      fail: ["comments/post-fail"],
    },
  };
});

registerEvent("comments/post-success", function (db, slug, { comment }) {
  const newDb = updateIn(db, ["comments", "data", slug], function (comments) {
    return [comment, ...comments];
  });
  return {
    db: assocAll(newDb, {
      "comments/postLoading": false,
      "comments/errors": null,
    }),
  };
});

registerEvent("comments/post-fail", function (db, { errors }) {
  return {
    db: assocAll(db, {
      "comments/errors": Object.entries(errors),
    }),
  };
});

registerEvent("comments/load-success", function (db, slug, { comments }) {
  return {
    db: assocAll(db, {
      "comments/loading": false,
      ["comments/data/" + slug]: comments,
    }),
  };
});

registerEvent("comments/load-fail", function (db, { errors }) {
  return {
    db: assocAll(db, {
      "comments/loading": false,
      "comments/errors": Object.entries(errors),
    }),
  };
});

registerEvent("comments/delete", function (db, { slug, id }) {
  return {
    db: updateIn(db, ["comments", "data", slug], function (comments) {
      return comments.filter((c) => c.id !== id);
    }),
    http: {
      method: "DELETE",
      url: "/api/articles/" + slug + "/comments/" + id,
    },
  };
});

registerQuery("comments/for-article", function (db, slug) {
  return getIn(db, ["comments", "data", slug], []);
});

registerQuery("comments/errors", function (db, slug) {
  return getIn(db, ["comments", "errors"], []);
});

registerQuery("comments/new", function (db) {
  return getIn(db, ["comments", "new"], "");
});

registerEvent("comments/update-value", function (db, value) {
  return {
    db: assocIn(db, ["comments", "new"], value),
  };
});
