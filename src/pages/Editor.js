import { useState } from "react";
import { useSubscription, dispatch } from "../reframe";
import { ErrorList } from "../components/ErrorList.js";

export function Editor() {
  const errors = useSubscription("articles/errors");
  const article = useSubscription("articles/active");
  const [title, setTitle] = useState(article.title);
  const [body, setBody] = useState(article.body);
  const [description, setDescription] = useState(article.description);
  const [tags, setTags] = useState((article.tagList || []).join(" "));
  return (
    <div className="editor-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-10 offset-md-1 col-xs-12">
            <ErrorList errors={errors} />
            <form>
              <fieldset className="form-group">
                <input
                  className="form-control form-control-lg"
                  type="text"
                  placeholder="Article Title"
                  defaultValue={article.title}
                  onChange={(e) => setTitle(e.target.value)}
                />
              </fieldset>
              <fieldset className="form-group">
                <input
                  className="form-control form-control-lg"
                  type="text"
                  placeholder="Article Title"
                  defaultValue={article.description}
                  onChange={(e) => setDescription(e.target.value)}
                />
              </fieldset>
              <fieldset className="form-group">
                <textarea
                  className="form-control form-control-lg"
                  rows="8"
                  placeholder="Write your article (in markdown)"
                  defaultValue={article.body}
                  onChange={(e) => setBody(e.target.value)}
                />
              </fieldset>
              {!article.slug && (
                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="text"
                    placeholder="Enter tags"
                    defaultValue={(article.tagList || []).join(" ")}
                    onChange={(e) => setTags(e.target.value)}
                  />
                </fieldset>
              )}
              <button
                className="btn bnt-lg btn-primary pull-xs-right"
                onClick={(e) => {
                  e.preventDefault();
                  dispatch("articles/upsert", {
                    slug: article.slug,
                    article: {
                      title,
                      description,
                      body,
                      tagList: !article.slug ? tags.split(" ") : undefined,
                    },
                  });
                }}
              >
                {article.slug ? "Update Article" : "Publish Article"}
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
