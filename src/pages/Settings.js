import { useSubscription, dispatch } from "../reframe";
import { ErrorList } from "../components/ErrorList";

export function Settings() {
  const username = useSubscription("user/username");
  const email = useSubscription("user/email");
  const bio = useSubscription("user/bio");
  const image = useSubscription("user/image");
  const errors = useSubscription("user/errors");
  const loading = useSubscription("user/loading?");
  return (
    <div className="settings-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-6 offset-md-3 col-xs-12">
            <h1 className="text-xs-center">Your Settings</h1>
            <ErrorList errors={errors} />
            <form
              onSubmit={(e) => {
                e.preventDefault();
                dispatch("user/update", {
                  email,
                  bio,
                  image,
                  username,
                });
              }}
            >
              <fieldset className="form-group">
                <input
                  value={image}
                  className="form-control form-control-lg"
                  onChange={(e) =>
                    dispatch("user/update-field", "image", e.target.value)
                  }
                  type="text"
                  placeholder="URL of profile image"
                />
              </fieldset>
              <fieldset className="form-group">
                <input
                  value={username}
                  onChange={(e) =>
                    dispatch("user/update-field", "username", e.target.value)
                  }
                  className="form-control form-control-lg"
                  placeholder="Your Name"
                />
              </fieldset>
              <fieldset className="form-group">
                <textarea
                  rows="8"
                  value={bio}
                  onChange={(e) =>
                    dispatch("user/update-field", "bio", e.target.value)
                  }
                  className="form-control form-control-lg"
                  placeholder="Short bio about you"
                />
              </fieldset>
              <fieldset className="form-group">
                <input
                  value={email}
                  onChange={(e) =>
                    dispatch("user/update-field", "email", e.target.value)
                  }
                  className="form-control form-control-lg"
                  placeholder="Your Email"
                />
              </fieldset>
              <button
                className={
                  "btn btn-lg btn-primary pull-xs-right " +
                  (loading ? "disabled" : "")
                }
              >
                Update Settings
              </button>
            </form>
            <hr />
            <button
              onClick={() => dispatch("user/logout")}
              className={
                "btn btn-outline-danger " + (loading ? "disabled" : "")
              }
            >
              Or click here to logout.
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
