import { registerEvent, registerQuery } from "../reframe";
import { assocAll, assocIn, assoc, get, getIn } from "../utils";

registerQuery("favorites/loading?", function (db) {
  return getIn(db, ["favorites", "loading"], false);
});

registerEvent("favorites/toggle", function (db, slug) {
  const favorited = getIn(db, ["articles", "data", slug, "favorited"], false);
  return {
    db: assocIn(db, ["articles", "favortiesLoading"], true),
    http: {
      method: favorited ? "DELETE" : "POST",
      url: "/api/articles/" + slug + "/favorite",
      success: ["favorites/toggle-success"],
      fail: ["favorites/toggle-fail"],
    },
  };
});

registerEvent(
  "favorites/toggle-success",
  function (db, { article }) {
    const { slug, favorited, favoritesCount } = article;
    return {
      db: assocAll(db, {
        "favorites/loading": false,
        ["articles/data/" + slug + "/favorited"]: favorited,
        ["articles/data/" + slug + "/favoritesCount"]: favoritesCount,
      }),
    };
  }
);
