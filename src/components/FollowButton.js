import { useSubscription, dispatch } from "../reframe";

export function FollowButton({ username }) {
  const loading = useSubscription("follow/loading?");
  const { following } = useSubscription("profile/data");
  function getFollowExtraClass() {
    if (loading) {
      return "disabled";
    }
    return "";
  }
  return (
    <button
      className={
        "btn btn-sm action-btn btn-outline-secondary " + getFollowExtraClass()
      }
      onClick={() => dispatch("follow/toggle", username)}
    >
      <i className={following ? "ion-minus-round" : "ion-plus-round"} />
      <span>{following ? " Unfollow " + username : " Follow " + username}</span>
    </button>
  );
}
