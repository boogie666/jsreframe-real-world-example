import { registerEvent, registerQuery } from "../reframe";
import { assocAll, assocIn, assoc, get, getIn } from "../utils";

registerEvent("tags/load", function (db) {
  return {
    db: assocIn(db, ["tags", "loading?"], true),
    http: {
      method: "GET",
      url: "/api/tags",
      success: ["tags/load-success"],
      fail: ["tags/load-fail"],
    },
  };
});

registerEvent("tags/load-success", function (db, result) {
  return {
    db: assocAll(db, { "tags/data": result.tags, "tags/loading": false }),
  };
});

registerQuery("tags/loading?", function (db) {
  return getIn(db, ["tags", "loading"], false);
});

registerQuery("tags/data", function (db) {
  return getIn(db, ["tags", "data"], []);
});
