export function assoc(obj, key, value) {
  if (!obj) {
    obj = {};
  }
  return {
    ...obj,
    [key]: value,
  };
}

export function assocIn(obj, path, value) {
  const [first, ...rest] = path;
  if (!obj) {
    obj = {};
  }

  if (rest.length === 0) {
    return assoc(obj, first, value);
  }
  return assoc(obj, first, assocIn(obj[first], rest, value));
}

export function assocAll(obj, data) {
  const paths = Object.entries(data);
  return paths.reduce(function (result, [path, value]) {
    return assocIn(result, path.split("/"), value);
  }, obj);
}

export function get(obj, key, def = null) {
  if (!obj) {
    return def;
  }
  return obj[key] || def;
}

export function getIn(obj, path, def = null) {
  const [first, ...rest] = path;
  if (rest.length === 0) {
    return get(obj, first, def);
  }
  return getIn(get(obj, first, def), rest, def);
}

export function update(obj, key, f) {
  if (!obj) {
    obj = {};
  }
  return {
    ...obj,
    [key]: f(obj[key]),
  };
}

export function updateIn(obj, path, f) {
  const value = getIn(obj, path);
  return assocIn(obj, path, f(value));
}
