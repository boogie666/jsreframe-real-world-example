import { useSubscription, dispatch } from "../reframe";
import { ErrorList } from "../components/ErrorList";
import { ArticlesList } from "../components/Article";
import { FollowButton } from "../components/FollowButton";

export function Profile() {
  const profileLoading = useSubscription("profile/loading?");
  const { image, username, bio, following } = useSubscription("profile/data");
  const { author, favorited } = useSubscription("filter/data");
  const loggedUser = useSubscription("user/username");
  const loggedIn = useSubscription("user/logged-in?");
  const articlesLoading = useSubscription("articles/loading?");
  const articles = useSubscription("articles/data");
  if (profileLoading) {
    return null;
  }
  return (
    <div className="profile-page">
      <div className="user-info">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-md-10 offset-md-1">
              <img className="user-img" src={image} alt="user image" />
              <h4>{username}</h4>
              <p>{bio}</p>
              {username === loggedUser ? (
                <a
                  className="btn btn-sm btn-outline-secondary action-btn"
                  href="/#/settings"
                >
                  <i className="ion-gear-a" /> Edit Profile Settings
                </a>
              ) : (
                loggedIn && <FollowButton username={username} />
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-xs-12 col-md-10 offset-md-1">
            <div className="articles-toggle">
              <ul className="nav nav-pills outline-active">
                <li className="nav-item">
                  <a
                    className={
                      "nav-link " + (author && !favorited ? "active" : null)
                    }
                    href={"/#/profile/" + username}
                  >
                    My Articles
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className={"nav-link " + (favorited ? "active" : null)}
                    href={"/#/profile/" + username + "/favorites"}
                  >
                    Favorites
                  </a>
                </li>
              </ul>
            </div>
            <ArticlesList articles={articles} loading={articlesLoading} />
          </div>
        </div>
      </div>
    </div>
  );
}
