import { registerEvent, registerQuery } from "../reframe";
import { assocAll, assocIn, assoc, get, getIn } from "../utils";

registerEvent("user/register", function (db, registration) {
  return {
    db: assocIn(db, ["user", "loading"], false),
    http: {
      url: "/api/users",
      method: "POST",
      body: { user: registration },
      success: ["user/success"],
      fail: ["user/fail"],
    },
  };
});

registerEvent("user/success", function (db, result) {
  return {
    db: assoc(db, "user", { ...result.user, loading: false, errors: [] }),
    localStorage: {
      method: "write",
      key: "user/data",
      value: JSON.stringify(result),
    },
    nav: "#/",
  };
});

registerEvent("user/fail", function (db, error) {
  return {
    db: assocAll(db, {
      "user/loading": false,
      "user/errors": Object.entries(error.errors),
    }),
  };
});

registerEvent("user/login", function (db, credentials) {
  return {
    db: assocIn(db, ["user", "loading"], true),
    http: {
      url: "/api/users/login",
      method: "POST",
      body: { user: credentials },
      success: ["user/success"],
      fail: ["user/fail"],
    },
  };
});

registerEvent("user/update", function (db, data) {
  return {
    db: assocIn(db, ["user", "loading"], false),
    http: {
      url: "/api/user",
      method: "PUT",
      body: { user: data },
      success: ["user/success"],
      fail: ["user/fail"],
    },
  };
});

registerEvent("user/logout", function (db) {
  return {
    db: assoc(db, "user", null),
    localStorage: {
      method: "delete",
      key: "user/data",
    },
    nav: "#/",
  };
});

registerQuery("user/logged-in?", function (db) {
  return getIn(db, ["user", "token"]) != null;
});

registerQuery("user/email", function (db) {
  return getIn(db, ["user", "email"], "");
});

registerQuery("user/bio", function (db) {
  return getIn(db, ["user", "bio"], "");
});

registerQuery("user/image", function (db) {
  return getIn(db, ["user", "image"], "");
});

registerQuery("user/username", function (db) {
  return getIn(db, ["user", "username"], "");
});

registerQuery("user/password", function (db) {
  return getIn(db, ["user", "password"], "");
});

function capitalize(s) {
  let [f, ...r] = s.split("");
  return f.toUpperCase() + r.join("");
}

registerEvent("user/update-field", function (db, field, value) {
  return { db: assocIn(db, ["user", field], value) };
});

registerQuery("user/value", function (db, value) {
  return getIn(db, ["user", value], "");
});

registerQuery("user/data", function (db, value) {
  return get(db, "user", {});
});

registerQuery("user/loading?", function (db) {
  return getIn(db, ["user", "loading"], false);
});

registerQuery("user/errors", function (db) {
  const errors = getIn(db, ["user", "errors"], []);
  return errors.map(function ([key, message]) {
    return `${capitalize(key)} - ${message}`;
  });
});

registerEvent("user/init-userdata", function (db, userdata) {
  const { user } = JSON.parse(userdata) || {};
  return {
    db: assocAll(db, {
      user: user,
      "app/loading": false,
      "filter/feed": true,
    }),
  };
});
