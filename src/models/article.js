import { registerEvent, registerQuery } from "../reframe";
import { assocAll, assocIn, assoc, get, getIn, updateIn } from "../utils";

function makeParams(params) {
  return Object.entries(params)
    .map(function ([k, v]) {
      return k + "=" + v;
    })
    .join("&");
}

registerEvent("articles/load", function (db, params) {
  return {
    db: assocAll(db, {
      "articles/loading": true,
      "filter/offset": params.offset,
      "filter/tag": params.tag,
      "filter/author": params.author,
      "filter/favorited": params.favorited,
      "filter/feed": false,
    }),
    http: {
      method: "GET",
      url: "/api/articles?" + makeParams(params),
      success: ["articles/load-success"],
      fail: ["articles/load-fail"],
    },
  };
});

registerEvent("articles/load-feed", function (db, params) {
  return {
    db: assocAll(db, {
      "articles/loading": true,
      "filter/offset": params.offset,
      "filter/tag": params.tag,
      "filter/author": params.author,
      "filter/favorited": params.favorited,
      "filter/feed": true,
    }),
    http: {
      method: "GET",
      url: "/api/articles/feed?" + makeParams(params),
      success: ["articles/load-success"],
      fail: ["articles/load-fail"],
    },
  };
});

function indexBy(xs, key) {
  return xs.reduce(function (result, item) {
    result[item[key]] = item;
    return result;
  }, {});
}

registerEvent("articles/load-fail", function (db, error) {
  console.error(error);
  return {};
});

registerEvent("articles/load-success", function (db, result) {
  return {
    db: assocAll(db, {
      "articles/loading": false,
      "articles/data": indexBy(result.articles, "slug"),
      "articles/count": result.count,
    }),
  };
});

registerEvent("articles/get-by-slug", function (db, params) {
  return {
    db: assocIn(db, ["articles", "loading"], true),
    http: {
      method: "GET",
      url: "/api/articles/" + params.slug,
      success: ["articles/get-by-slug-success"],
      fail: ["articles/load-fail"],
    },
  };
});

registerEvent("articles/get-by-slug-success", function (db, result) {
  return {
    db: assocAll(db, {
      "articles/loading": false,
      ["articles/data/" + result.article.slug]: result.article,
    }),
  };
});

registerQuery("articles/loading?", function (db) {
  return getIn(db, ["articles", "loading"], false);
});

registerQuery("articles/count", function (db) {
  return getIn(db, ["articles", "count"], false);
});

registerQuery("articles/data", function (db) {
  const articlesIndex = getIn(db, ["articles", "data"], {});
  return Object.values(articlesIndex).sort(function (a, b) {
    return b.epoch - a.epoch;
  });
});

registerQuery("articles/active", function (db) {
  const slug = getIn(db, ["activeArticle", "slug"], null);
  if (!slug) {
    return {};
  }
  return getIn(db, ["articles", "data", slug], {});
});

registerEvent("articles/delete", function (db, slug) {
  const newDb = updateIn(db, ["articles", "data"], function (data) {
    let newData = { ...data };
    delete newData[slug];
    return newData;
  });
  return {
    db: assocIn(newDb, ["articles", "active"], null),
    http: {
      method: "DELETE",
      url: "/api/articles/" + slug,
    },
  };
});

registerQuery("articles/errors", function (db) {
  return getIn(db, ["articles", "errors"], []);
});

registerEvent("articles/upsert", function (db, { slug, article }) {
  return {
    db: assocIn(db, ["editor", "loading"], true),
    http: {
      method: slug ? "PUT" : "POST",
      url: slug ? "/api/articles/" + slug : "/api/articles",
      body: { article },
      success: ["articles/upsert-success"],
      fail: ["articles/upsert-fail"],
    },
  };
});

registerEvent("articles/upsert-success", function (db, { article }) {
  const me = get(db, "user");
  console.log(db);
  return {
    db: assocAll(db, {
      "editor/loading": false,
      ["articles/data/" + article.slug + "/author"]: me,
      "comments/data": null,
      "articles/errors": null,
    }),
    dispatchN: [
      { event: "articles/get-by-slug", params: [{ slug: article.slug }] },
      { event: "comments/load", params: [{ slug: article.slug }] },
    ],
    nav: "#/article/" + article.slug,
  };
});
