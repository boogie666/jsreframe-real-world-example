export function ErrorList({ errors }) {
  if (errors.length === 0) {
    return null;
  }
  return (
    <ul className="error-messages">
      {errors.map(function (e, i) {
        return <li key={i}>{e}</li>;
      })}
    </ul>
  );
}
