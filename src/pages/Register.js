import { useSubscription, dispatch } from "../reframe";
import { ErrorList } from "../components/ErrorList";

export function Register() {
  const username = useSubscription("user/username");
  const email = useSubscription("user/email");
  const password = useSubscription("user/password");
  const loading = useSubscription("user/loading?");
  const errors = useSubscription("user/errors");
  return (
    <div className="auth-page">
      <div className="container page">
        <div className="row">
          <div className="col-md-6 offset-md-3 col-xs-12">
            <h1 className="text-xs-center">Sign Up</h1>
            <p className="text-xs-center">
              <a>Have an account?</a>
            </p>
            <ErrorList errors={errors} />
            <form
              onSubmit={(e) => {
                e.preventDefault();
                dispatch("user/register", { username, email, password });
              }}
            >
              <fieldset className="form-group">
                <input
                  className="form-control form-control-lg"
                  value={username}
                  onChange={(e) =>
                    dispatch("user/update-field", "username", e.target.value)
                  }
                  placeholder="Your name"
                />
              </fieldset>
              <fieldset className="form-group">
                <input
                  value={email}
                  className="form-control form-control-lg"
                  onChange={(e) =>
                    dispatch("user/update-field", "email", e.target.value)
                  }
                  type="text"
                  placeholder="Email"
                />
              </fieldset>
              <fieldset className="form-group">
                <input
                  value={password}
                  onChange={(e) =>
                    dispatch("user/update-field", "password", e.target.value)
                  }
                  className="form-control form-control-lg"
                  type="password"
                  placeholder="Password"
                />
              </fieldset>
              <button
                className={
                  "btn btn-lg btn-primary pull-xs-right " +
                  (loading ? "disabled" : "")
                }
              >
                Sign Up
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
