import { useSubscription, dispatch } from "../reframe";

export function FavButton({ article }) {
  const { favorited, slug, favoritesCount } = article;
  const loading = useSubscription("favorites/loading?");
  function getExtraClass() {
    if (loading) {
      return "disabled";
    }
    if (!favorited) {
      return "btn-outline-primary";
    }
    return "";
  }
  return (
    <button
      className={"btn btn-primary btn-sm pull-xs-right " + getExtraClass()}
      onClick={() => dispatch("favorites/toggle", slug)}
    >
      <i className="ion-heart"> </i>
      <span className="counter">{favoritesCount}</span>
    </button>
  );
}
