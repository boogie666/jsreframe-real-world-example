import { useSubscription, dispatch, Provider } from "./reframe";
import { useEffect } from "react";
import "./effects";
import "./models/app.js";
import "./models/user.js";
import "./models/profile.js";
import "./models/article.js";
import "./models/tag.js";
import "./models/filter.js";
import "./models/favorites.js";
import "./models/follow.js";
import "./models/comments.js";
import { useNav } from "./nav";
import { Header } from "./components/Header";
import { Footer } from "./components/Footer";
import { Home } from "./pages/Home";
import { Login } from "./pages/Login";
import { Settings } from "./pages/Settings";
import { Register } from "./pages/Register";
import { Profile } from "./pages/Profile";
import { Article } from "./pages/Article";
import { Editor } from "./pages/Editor";

function renderPage(activePage) {
  switch (activePage) {
    case "home":
      return <Home />;
    case "article":
      return <Article />;
    case "settings":
      return <Settings />;
    case "register":
      return <Register />;
    case "profile":
      return <Profile />;
    case "login":
      return <Login />;
    case "editor":
      return <Editor />;
    default:
      return <Home />;
  }
}

export default function App() {
  const loading = useSubscription("app/loading?");
  const activePage = useSubscription("app/active-page");
  useNav();
  useEffect((_) => dispatch("app/init"), []);
  return (
    !loading && (
      <>
        <Header />
        {renderPage(activePage)}
        <Footer />
      </>
    )
  );
}
