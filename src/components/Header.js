import { useSubscription, dispatch } from "../reframe";
import "../models/user";

export function Header() {
  const loggedIn = useSubscription("user/logged-in?");
  const username = useSubscription("user/username");
  const activePage = useSubscription("app/active-page");
  const image = useSubscription("user/image");
  return (
    <nav className="navbar navbar-light">
      <div className="container">
        <a href="/#" className="navbar-brand">
          conduit
        </a>
        {!loggedIn ? (
          <ul className="nav navbar-nav pull-xs-right">
            <li className="nav-item">
              <a
                href="/#"
                className={
                  "nav-link " + (activePage === "home" ? "active" : "")
                }
              >
                Home
              </a>
            </li>
            <li className="nav-item">
              <a
                href="/#/login"
                className={
                  "nav-link " + (activePage === "login" ? "active" : "")
                }
              >
                Sign In
              </a>
            </li>
            <li className="nav-item">
              <a
                href="/#/register"
                className={
                  "nav-link " + (activePage === "register" ? "active" : "")
                }
              >
                Sign Up
              </a>
            </li>
          </ul>
        ) : (
          <ul className="nav navbar-nav pull-xs-right">
            <li className="nav-item">
              <a
                href="/#"
                className={
                  "nav-link " + (activePage === "home" ? "active" : "")
                }
              >
                Home
              </a>
            </li>
            <li className="nav-item">
              <a
                href="/#/editor/new"
                className={
                  "nav-link " + (activePage === "editor" ? "active" : "")
                }
              >
                <i className="ion-compose">New Article</i>
              </a>
            </li>
            <li className="nav-item">
              <a
                href="/#/settings"
                className={
                  "nav-link " + (activePage === "settings" ? "active" : "")
                }
              >
                <i className="ion-gear-a">Settings</i>
              </a>
            </li>

            <li className="nav-item">
              <a
                href={"/#/profile/" + username}
                className={
                  "nav-link " + (activePage === "profile" ? "active" : "")
                }
              >
                <img className="user-pic" src={image} alt="user image" />
              </a>
            </li>
          </ul>
        )}
      </div>
    </nav>
  );
}
