import { useSubscription, dispatch } from "../reframe";
import { FavButton } from "./FavButton.js";
import { TagList } from "./TagList.js";

function ArticlesPreview({ article }) {
  const { description, slug, createdAt, title, author, favorited, tagList } =
    article;
  const loggedIn = useSubscription("user/logged-in?");
  const currentUser = useSubscription("user/username");

  return (
    <div className="article-preview">
      <div className="article-meta">
        <a href={"#/profile/user-id/" + author.username}>
          <img src={author.image} alt="user image" />
        </a>

        <div className="info">
          <a className="author" href={"#/profile/" + author.username}>
            {author.username}
          </a>
          <span className="date">{createdAt}</span>
        </div>
        {!loggedIn || author.username === currentUser ? null : (
          <FavButton article={article} />
        )}
      </div>
      <a className="preview-link" href={"#/article/" + slug}>
        <h1>{title}</h1>
        <p>{description}</p>
        <span>Read more ...</span>
        <TagList tags={tagList} />
      </a>
    </div>
  );
}

export function ArticlesList({ articles, loading }) {
  function renderArticles(articles) {
    if (articles.length === 0) {
      return (
        <div className="article-preview">
          <p>No articles here... yet.</p>
        </div>
      );
    } else {
      return articles.map((a) => <ArticlesPreview key={a.slug} article={a} />);
    }
  }
  return loading ? (
    <div className="article-preview">
      <p>Loading Articles</p>
    </div>
  ) : (
    renderArticles(articles)
  );
}
