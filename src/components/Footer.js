export function Footer() {
  return (
    <footer>
      <div className="container">
        <a href="/#" className="logo-front">
          conduit
        </a>
        <span className="attribution">
          An interactive learning project from{" "}
          <a href="https://thinkster.io">Thinkster</a>. Code & design licenced
          under MIT.
        </span>
      </div>
    </footer>
  );
}
