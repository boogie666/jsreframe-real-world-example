import { registerQuery } from "../reframe";
import { get } from "../utils";

registerQuery("filter/data", function (db) {
    return get(db, "filter", { offset: 0, feed: false });
});

