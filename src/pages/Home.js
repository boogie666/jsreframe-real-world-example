import { useSubscription, dispatch } from "../reframe";
import { ErrorList } from "../components/ErrorList";
import { ArticlesList } from "../components/Article";
import "../models/user";
import "../models/filter";
import "../models/article";

function Banner() {
  return (
    <div className="banner">
      <div className="container">
        <h1 className="logo-front">conduit</h1>
        <p>A place to share your knowledge</p>
      </div>
    </div>
  );
}

function ArticlesPagination() {
  const articlesLoading = useSubscription("articles/loading?");
  const articleCount = useSubscription("articles/count");
  const filter = useSubscription("filter/data");
  const range = new Array(Math.floor(articleCount / 10))
    .fill(0)
    .map((x, i) => i);
  return (
    <ul className="pagination">
      {range.map(function (i) {
        let active = i * 10 === filter.offset ? "active" : "";
        return (
          <li key={i} className={`page-item ${active}`}>
            <a className="page-link">{i + 1}</a>
          </li>
        );
      })}
    </ul>
  );
}

export function Home() {
  const articlesLoading = useSubscription("articles/loading?");
  const articles = useSubscription("articles/data");
  const loggedIn = useSubscription("user/logged-in?");
  const filter = useSubscription("filter/data");
  const tagsLoading = useSubscription("tags/loading?");
  const tags = useSubscription("tags/data");
  function getFeedArticles(e, params) {
    e.preventDefault();
    dispatch("articles/load-feed", params);
  }
  function getArticles(e, params) {
    e.preventDefault();
    dispatch("articles/load", params);
  }
  return (
    <div className="home-page">
      {!loggedIn ? <Banner /> : null}
      <div className="container">
        <div className="row">
          <div className="col-md-9">
            <div className="feed-toggle">
              <ul className="nav nav-pills outline-active">
                {!loggedIn ? null : (
                  <li className="nav-item">
                    <a
                      href="/#"
                      className={
                        "nav-link " +
                        (!filter.tag && filter.feed ? " active" : "")
                      }
                      onClick={(e) =>
                        getFeedArticles(e, { offset: 0, limit: 10 })
                      }
                    >
                      Your Feed
                    </a>
                  </li>
                )}
                <li className="nav-item">
                  <a
                    href="/#"
                    className={
                      "nav-link " +
                      (!filter.tag && !filter.feed ? " active" : "")
                    }
                    onClick={(e) => getArticles(e, { offset: 0, limit: 10 })}
                  >
                    Global Feed
                  </a>
                </li>
                {!filter.tag ? null : (
                  <li className="nav-item">
                    <a className="nav-link active">
                      <i className="ion-pound" /> {filter.tag}
                    </a>
                  </li>
                )}
              </ul>
            </div>
            <ArticlesList articles={articles} loading={articlesLoading} />
            <ArticlesPagination />
          </div>
          <div className="col-md-3">
            <div className="sidebar">
              <p>Popular Tags</p>
              {tagsLoading ? (
                <p>Loading tags...</p>
              ) : (
                <div className="tag-list">
                  {tags.map(function (t, i) {
                    return (
                      <a
                        key={i}
                        className="tag-pill tag-default"
                        href="#/"
                        onClick={(e) =>
                          getArticles(e, { limit: 10, offset: 0, tag: t })
                        }
                      >
                        {t}
                      </a>
                    );
                  })}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
