import React from "react";

const context = {
  db: null,
  listeners: [],
  effectHandlers: {},
  eventHandlers: {},
  queryHandlers: {},
};


export function setState(newState) {
  context.db = newState;
  for (const l of context.listeners) {
    l(newState);
  }
}

function subscribe(listener) {
  context.listeners.push(listener);
  return function () {
    context.listeners = context.listeners.filter((l) => l === listener);
  };
}

export function registerEvent(name, fn) {
  context.eventHandlers[name] = fn;
}

export function registerEffect(name, fn) {
  context.effectHandlers[name] = fn;
}

export function registerQuery(name, fn) {
  context.queryHandlers[name] = fn;
}

export function dispatch(e, ...params) {
  const handler = context.eventHandlers[e];
  if (!handler) {
    throw new Error(`Event ${e} does not have a registered event handler`);
  }
  const effects = handler(context.db, ...params);
  for (const [e, payload] of Object.entries(effects)) {
    const effectHandler = context.effectHandlers[e];
    if (!handler) {
      throw new Error(`Effect ${e} does not have a registered effect handler`);
    }
    effectHandler(context, payload);
  }
}

const ReframeContext = React.createContext(context);

export function useSubscription(name, ...params) {
  const db = React.useContext(ReframeContext);
  const handler = context.queryHandlers[name];
  if (!handler) {
    throw new Error(
      `Subscription ${name} does not have a registered query handler`
    );
  }
  return handler(db, ...params);
}

export function Provider({ children }) {
  let [db, setDb] = React.useState(context.db);
  React.useEffect((_) => subscribe(setDb), []);
  return (
    <ReframeContext.Provider value={db}>{children}</ReframeContext.Provider>
  );
}

