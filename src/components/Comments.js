import { useSubscription, dispatch } from "../reframe";
import { ErrorList } from "./ErrorList";

export function Comments({ article }) {
  const errors = useSubscription("comments/errors");
  const loggedIn = useSubscription("user/logged-in?");
  const username = useSubscription("user/username");
  const { image } = useSubscription("user/data");
  const comment = useSubscription("comments/new");
  const comments = useSubscription("comments/for-article", article.slug);
  const loading = useSubscription("comments/loading?");
  const postLoading = useSubscription("comments/post-loading?");
  return (
    <div className="row">
      <div className="col-xs-12 col-sm-8 offset-md-2">
        <ErrorList errors={errors} />
        {loggedIn ? (
          <form className="card comment-form">
            <div className="card-block">
              <textarea
                className="form-control"
                placeholder="Write a comment..."
                rows="3"
                value={comment}
                onChange={(e) =>
                  dispatch("comments/update-value", e.target.value)
                }
              />
            </div>
            <div className="card-footer">
              <img
                className="comment-author-img"
                src={image}
                alt="user image"
              />
              <button
                className={
                  "btn btn-sm btn-primary " + (postLoading ? "disabled" : "")
                }
                onClick={(e) => {
                  e.preventDefault();
                  dispatch("comments/post", {
                    slug: article.slug,
                    body: comment,
                  });
                }}
              >
                Post Comment
              </button>
            </div>
          </form>
        ) : (
          <p>
            <a href="/#/register">Sign up</a>
            {" or "}
            <a href="/#/login">Sign in</a>
            {" to add comments to this article."}
          </p>
        )}
        {loading ? (
          <div>
            <p>Loading comments...</p>
          </div>
        ) : (
          comments.map(function ({ id, createdAt, body, author }) {
            return (
              <div key={id} className="card">
                <div className="card-block">
                  <p className="card-text">{body}</p>
                </div>
                <div className="card-footer">
                  <a
                    className="comment-author"
                    href={"#/profile/" + author.username}
                  >
                    <img
                      className="comment-author-img"
                      src={author.image}
                      alt="user image"
                    />
                  </a>{" "}
                  <a
                    className="comment-author"
                    href={"#/profile/" + author.username}
                  >
                    {author.username}
                  </a>
                  <span className="date-posted">{createdAt}</span>
                  {username !== author.username ? null : (
                    <span
                      className="mod-options"
                      onClick={() =>
                        dispatch("comments/delete", { slug: article.slug, id })
                      }
                    >
                      <i className="ion-trash-a" />
                    </span>
                  )}
                </div>
              </div>
            );
          })
        )}
      </div>
    </div>
  );
}
